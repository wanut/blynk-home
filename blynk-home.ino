#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <DHT.h>
#define BLYNK_PRINT Serial
#define DHTPIN D5
#define DHTTYPE DHT22

//insert your blynk token.
char auth[] = ".........";
//insert your home wifi ssid.
char ssid[] = ".........";
//insert your home wifi password.
char pass[] = ".........";

float temp,hum;

BlynkTimer timer;
DHT dht(DHTPIN,DHTTYPE);

void sendSensor()
{
  Blynk.virtualWrite(V1,temp);
  Blynk.virtualWrite(V2,hum);
}

void setup()
{
  Serial.begin(9600);
  Blynk.begin(auth, ssid, pass);
  dht.begin();
  //timer 1000 = 1 second.
  timer.setInterval(2000L,sendSensor);
}

void loop()
{
  Blynk.run();
  timer.run();
  temp = dht.readTemperature();
  hum = dht.readHumidity();
}